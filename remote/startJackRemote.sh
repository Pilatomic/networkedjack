#!/bin/bash

JACK_SOUNDCARD=0
SERVER_IP=192.168.103.1
SLAVE_NAME=telephone

export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/dbus/system_bus_socket
/usr/bin/jackd -S -R -P70 -dalsa -dhw:$JACK_SOUNDCARD -r44100 -p1024 -n2 -s -Xseq &
jack_wait -w
jack_load netadapter -i "-a $SERVER_IP -c -l 1 -n $SLAVE_NAME"



