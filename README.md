Allows transmission of sound over ethernet using Jack2.

The remote device (here a Raspberry Pi) receive sound from a server (another computer running Jack).


# Installation
## Remote
> sudo apt-get install jackd2

> cd remote

Edit `startJackRemote.sh` : 
> nano startJackRemote.sh

Change the following line to select the soundcard, server IP and displayed name for this remote device: 
> JACK_SOUNDCARD=0
> SERVER_IP=192.168.103.1
> SLAVE_NAME=remote


On Raspberry Pi : 
0 for internal soundcard
1 for external (USB) soundcard

Now setup script execution on boot.
Edit `/etc/rc.local`
> sudo nano /etc/rc.local

Before `exit 0`, add the following line : 
> su pi -c "/home/pi/networkedjack/remote/startJackRemote.sh"

Now restart your server computer
> sudo reboot


## Server
> sudo apt-get install jackd2 pulseaudio-module-jack

> cd server


> ./startJackServer.sh

Then use a program such as QJackCtl to connect local sound I/O to remotes
Enjoy :-) 
